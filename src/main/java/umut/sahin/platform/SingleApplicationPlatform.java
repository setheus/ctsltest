package umut.sahin.platform;

import org.apache.commons.lang3.Validate;
import umut.sahin.exception.PlayerAlreadyRegisteredException;
import umut.sahin.exception.PlayerNotFoundException;
import umut.sahin.model.AbstractMessage;
import umut.sahin.model.ConfirmationMessage;
import umut.sahin.model.Message;
import umut.sahin.model.StopMessage;
import umut.sahin.player.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Single App version of the Platform
 * Responsible for sending messages across players
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class SingleApplicationPlatform implements Platform {
    private final Map<String, Player> playerMap = new HashMap<>();
    private final String name;
    private boolean running = true;

    public SingleApplicationPlatform(String name) {
        Validate.notBlank(name, "SingleApplicationPlatform name is blank");
        this.name = name;
        System.out.println("SingleApplicationPlatform \"" + name + "\" created.\n\n");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void registerPlayer(Player player) {
        Validate.notNull(player, "Player is null");
        if (playerMap.containsKey(player.getUsername())) {
            throw new PlayerAlreadyRegisteredException("Player " + player.getUsername() + " is already registered to the platform");
        }
        playerMap.put(player.getUsername(), player);
        player.setPlatform(this);
    }

    private void checkPlayerIfRegistered(String senderUsername) {
        if (!playerMap.containsKey(senderUsername)) {
            throw new PlayerNotFoundException("Player " + senderUsername + " is not registered to the platform");
        }
    }

    @Override
    public void deliverMessage(AbstractMessage message) {
        message.validate();
        if (ConfirmationMessage.class.isInstance(message)) {
            ConfirmationMessage msg = ConfirmationMessage.class.cast(message);
            checkPlayerIfRegistered(msg.getSenderUsername());
            checkPlayerIfRegistered(msg.getReceiverUsername());
            Player receiver = playerMap.get(msg.getSenderUsername());
            receiver.receiveMessage(msg);
        } else if (Message.class.isInstance(message)) {
            Message msg = Message.class.cast(message);
            checkPlayerIfRegistered(msg.getSenderUsername());
            checkPlayerIfRegistered(msg.getReceiverUsername());
            Player receiver = playerMap.get(msg.getReceiverUsername());
            receiver.receiveMessage(msg);
        } else if (StopMessage.class.isInstance(message)) {
            stop();
        }
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public void stop() {
        running = false;
    }
}
