package umut.sahin.platform;

import umut.sahin.Application;
import umut.sahin.model.AbstractMessage;
import umut.sahin.model.ConfirmationMessage;
import umut.sahin.model.GreetingMessage;
import umut.sahin.model.Message;
import umut.sahin.player.Player;
import umut.sahin.player.RemotePlayer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.Objects;

/**
 * This is the Client for Distributed Platform.
 * This class creates a nio socket to server.
 * If there is a message on the outgoing messages sends it
 * Until the stop condition is met it will send the messages
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class ClientPlatform extends AbstractDistributedPlatform {
    private RemotePlayer localPlayer;

    public ClientPlatform() throws IOException {
        super();
    }

    @Override
    protected AbstractSelectableChannel createChannel() throws IOException {
        SocketChannel sc = SocketChannel.open();
        sc.configureBlocking(false);
        sc.connect(new InetSocketAddress(Application.PORT));
        return sc;
    }

    @Override
    protected int getSelectionForRegister() {
        return SelectionKey.OP_CONNECT;
    }

    @Override
    public String getName() {
        return "Client Platform";
    }

    @Override
    protected void afterMessageRead(SocketChannel channel, AbstractMessage message) {
        if (GreetingMessage.class.isInstance(message)) {
            RemotePlayer player = new RemotePlayer(message.getSenderUsername()).setChannel(channel);
            registerPlayer(player);
            if (outMessages.isEmpty()) {
                registerForRead(channel);
            } else {
                registerForWrite(channel);
            }
        } else if (ConfirmationMessage.class.isInstance(message)) {
            ConfirmationMessage msg = ConfirmationMessage.class.cast(message);
            localPlayer.receiveMessage(msg);
            registerForWrite(channel);
        } else if (Message.class.isInstance(message)) {
            Message msg = Message.class.cast(message);
            localPlayer.receiveMessage(msg);
            registerForWrite(channel);
        } else {
            running = false;
        }
    }

    @Override
    public void deliverMessage(AbstractMessage message) {
        if (!stopping && Objects.nonNull(message)) {
            outMessages.add(message);
        }
    }

    @Override
    public void registerPlayer(Player player) {
        if (!knownUsers.containsKey(player.getUsername())) {
            RemotePlayer remotePlayer = (RemotePlayer) player;
            knownUsers.put(player.getUsername(), remotePlayer);
            System.out.println("Player(" + remotePlayer.getUsername() + ") registered");
            remotePlayer.setPlatform(this);
            if (remotePlayer.getChannel() == null) {
                localPlayer = remotePlayer;
            }
        }
    }

    @Override
    protected void logSentMessage(AbstractMessage message) {
        if (ConfirmationMessage.class.isInstance(message)) {
            ConfirmationMessage msg = ConfirmationMessage.class.cast(message);
            System.out.println("# Confirmed Message\n" + msg.getSenderUsername() + "<- " +
                               msg.getMessage() + "|" + msg.getCounter() + "\n");
        } else if (Message.class.isInstance(message)) {
            Message msg = Message.class.cast(message);
            System.out.println("# Sent Message\n" + msg.getReceiverUsername() + "-> " + msg.getMessage() + "\n");
        }
    }
}
