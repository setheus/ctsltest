package umut.sahin.platform;

import umut.sahin.model.AbstractMessage;
import umut.sahin.model.GreetingMessage;
import umut.sahin.model.StopMessage;
import umut.sahin.player.Player;
import umut.sahin.player.RemotePlayer;
import umut.sahin.util.IOUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 * This is an abstract class for Client-Server Platform type for distributed version.
 * @author u.sahin
 * @since 12/06/2017
 */
public abstract class AbstractDistributedPlatform implements Runnable, Platform {

    protected static String SERVER_NAME = "Server";
    protected final ByteBuffer buffer = ByteBuffer.allocate(8096);
    protected final Selector selector;
    protected Map<String, RemotePlayer> knownUsers = new HashMap<>();
    protected LinkedList<AbstractMessage> outMessages = new LinkedList<>();
    protected volatile boolean running;
    protected volatile boolean stopping;

    public AbstractDistributedPlatform() throws IOException {
        AbstractSelectableChannel channel = createChannel();
        selector = Selector.open();
        channel.register(selector, getSelectionForRegister());
        System.out.println("\n\n################\n" + getName() + " has been started...\n");
    }

    protected abstract AbstractSelectableChannel createChannel() throws IOException;

    protected abstract int getSelectionForRegister();

    @Override
    public abstract void registerPlayer(Player player);

    @Override
    public abstract void deliverMessage(AbstractMessage message);

    @Override
    public abstract String getName();

    @Override
    public void stop() {
        stopping = true;
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    public synchronized void start() {
        Thread thread = new Thread(this);
        running = true;
        thread.start();
    }

    private void closeAll() {
        for (SelectionKey key : selector.keys()) {
            try {
                key.channel().close();
            } catch (IOException ignored) {
            }
        }
        try {
            selector.close();
        } catch (IOException ignored) {
        }
    }

    @Override
    public void run() {
        while (running) {
            try {
                selector.select(1000);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                if (!key.isValid()) {
                    continue;
                }
                try {
                    handleSelectorKey(key);
                } catch (Exception e) {
                    e.printStackTrace();
                    key.cancel();
                }
            }
        }
        closeAll();
        System.out.println("\n" + getName() + " is closing...\n");
    }

    private void handleSelectorKey(SelectionKey key) throws Exception {
        if (key.isConnectable()) {
            connect(key);
        } else if (key.isAcceptable()) {
            accept(key);
        } else if (key.isReadable()) {
            read(key);
        } else if (key.isWritable()) {
            write(key);
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);
        registerForRead(socketChannel);
    }

    private void connect(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        try {
            channel.finishConnect();
            channel.configureBlocking(false);
            RemotePlayer player = knownUsers.values()
                                            .stream()
                                            .filter(rp -> rp.getChannel() == null)
                                            .findFirst()
                                            .orElseThrow(() -> new RuntimeException("Local player not registered"));
            outMessages.addFirst(new GreetingMessage().setSenderUsername(player.getUsername()));
            registerForWrite(channel);
        } catch (IOException e) {
            e.printStackTrace();
            key.channel().close();
            key.cancel();
        }
    }

    private void read(SelectionKey key) throws Exception {
        SocketChannel channel = (SocketChannel) key.channel();
        buffer.clear();
        int readCount = channel.read(buffer);

        if (readCount == -1) {
            registerForWrite(channel);
            return;
        }

        byte[] data = new byte[buffer.position()];
        System.arraycopy(buffer.array(), 0, data, 0, buffer.position());
        AbstractMessage message = IOUtils.deserializeObject(data);
        afterMessageRead(channel, message);
    }

    protected abstract void afterMessageRead(SocketChannel channel, AbstractMessage message);

    private void write(SelectionKey key) throws Exception {
        SocketChannel channel = (SocketChannel) key.channel();

        AbstractMessage message = outMessages.poll();

        if (message != null) {
            if (stopping) {
                boolean stopMessage = StopMessage.class.isInstance(message);
                if (stopMessage) {
                    sendMessage(channel, message);
                }
                registerForWrite(channel);
                if (stopMessage) {
                    running = false;
                }
            } else {
                sendMessage(channel, message);
                logSentMessage(message);
                registerForRead(channel);
            }
        }
    }

    protected abstract void logSentMessage(AbstractMessage message);

    private void sendMessage(SocketChannel channel, Object msg) {
        byte[] bytes;
        try {
            bytes = IOUtils.serializeObject(msg);
        } catch (Exception e) {
            return;
        }
        buffer.clear();
        buffer.put(bytes);
        buffer.flip();
        try {
            channel.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void registerForWrite(SelectableChannel channel) {
        registerForWrite(channel, null);
    }

    protected void registerForWrite(SelectableChannel channel, AbstractMessage message) {
        try {
            if (message != null) {
                outMessages.add(message);
            }
            if (outMessages.isEmpty()) {
                channel.register(selector, SelectionKey.OP_READ);
            } else {
                channel.register(selector, SelectionKey.OP_WRITE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void registerForRead(SelectableChannel channel) {
        try {
            channel.register(selector, SelectionKey.OP_READ);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
