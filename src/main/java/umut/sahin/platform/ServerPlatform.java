package umut.sahin.platform;

import umut.sahin.Application;
import umut.sahin.model.AbstractMessage;
import umut.sahin.model.ConfirmationMessage;
import umut.sahin.model.GreetingMessage;
import umut.sahin.model.Message;
import umut.sahin.player.Player;
import umut.sahin.player.RemotePlayer;
import umut.sahin.util.IOUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;

/**
 * This is the Server for Distributed Platform.
 * This class accepts connections from clients.
 * Only delivers messages across clients
 * If there is a stop message sent it will broadcast it and stop working
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class ServerPlatform extends AbstractDistributedPlatform {

    public ServerPlatform() throws IOException {
        super();
    }

    @Override
    protected AbstractSelectableChannel createChannel() throws IOException {
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);
        ssc.socket().bind(new InetSocketAddress(Application.PORT));
        return ssc;
    }

    @Override
    protected int getSelectionForRegister() {
        return SelectionKey.OP_ACCEPT;
    }

    @Override
    public String getName() {
        return "Server Platform";
    }

    @Override
    public void deliverMessage(AbstractMessage message) {
    }

    @Override
    public void registerPlayer(Player player) {
        if (!knownUsers.containsKey(player.getUsername())) {
            RemotePlayer remotePlayer = (RemotePlayer) player;
            knownUsers.put(player.getUsername(), remotePlayer);
            System.out.println("Player(" + remotePlayer.getUsername() + ") : " + IOUtils.getRemoteAddress(remotePlayer));
            outMessages.add(new GreetingMessage().setSenderUsername(SERVER_NAME));
            registerForWrite(remotePlayer.getChannel());
        }
    }

    @Override
    protected void afterMessageRead(SocketChannel channel, AbstractMessage message) {
        if (GreetingMessage.class.isInstance(message)) {
            RemotePlayer player = new RemotePlayer(message.getSenderUsername()).setChannel(channel);
            registerPlayer(player);
        } else if (ConfirmationMessage.class.isInstance(message)) {
            if (knownUsers.containsKey(message.getSenderUsername())) {
                RemotePlayer remotePlayer = knownUsers.get(message.getSenderUsername());
                registerForWrite(remotePlayer.getChannel(), message);
            }
        } else if (Message.class.isInstance(message)) {
            Message msg = Message.class.cast(message);
            if (knownUsers.containsKey(msg.getReceiverUsername())) {
                RemotePlayer remotePlayer = knownUsers.get(msg.getReceiverUsername());
                registerForWrite(remotePlayer.getChannel(), message);
            } else {
                registerForRead(channel);
            }
        } else {
            stop();
            knownUsers.keySet()
                      .stream()
                      .filter(user -> !user.equals(message.getSenderUsername()))
                      .forEach(user -> {
                          RemotePlayer remotePlayer = knownUsers.get(user);
                          registerForWrite(remotePlayer.getChannel(), message);
                      });
        }
    }

    @Override
    protected void logSentMessage(AbstractMessage message) {
        if (Message.class.isInstance(message) && !ConfirmationMessage.class.isInstance(message)) {
            Message msg = Message.class.cast(message);
            System.out.println("# Delivered Message >>>\n" + msg.getSenderUsername() + " to " +
                               msg.getReceiverUsername() + ": " + msg.getMessage() + "\n");
        }
    }
}
