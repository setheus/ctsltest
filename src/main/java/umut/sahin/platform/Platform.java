package umut.sahin.platform;

import umut.sahin.model.AbstractMessage;
import umut.sahin.player.Player;

/**
 * Platform interface for messaging.
 * 4 basic functionality: registering player, delivering message, checking if running and stopping
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public interface Platform {
    String getName();

    void registerPlayer(Player player);

    void deliverMessage(AbstractMessage message);

    boolean isRunning();

    void stop();
}
