package umut.sahin;

import umut.sahin.logic.StopCondition;
import umut.sahin.platform.ClientPlatform;
import umut.sahin.platform.ServerPlatform;
import umut.sahin.platform.SingleApplicationPlatform;
import umut.sahin.player.Player;
import umut.sahin.player.RemotePlayer;

import java.io.IOException;

/**
 * @author u.sahin
 * @since 12/06/2017
 */
public class Application {

    public static final int PORT = 6000;

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length == 0) {
            System.out.println("No mode selected");
            return;
        }
        ClientPlatform clientPlatform;
        switch (args[0]) {
            case "SINGLE":
                singleAppMode();
                break;
            case "SERVER":
                new ServerPlatform().start();
                break;
            case "CLIENT-RECEIVER":
                clientPlatform = new ClientPlatform();
                Player receiver = new RemotePlayer("receiver");
                clientPlatform.registerPlayer(receiver);
                clientPlatform.start();
                break;
            case "CLIENT-SENDER":
                clientPlatform = new ClientPlatform();
                Player sender = new RemotePlayer("sender").setStopCondition(new StopCondition(10));
                clientPlatform.registerPlayer(sender);
                clientPlatform.start();
                int i = 1;
                while (clientPlatform.isRunning()) {
                    sender.sendMessage("receiver", "Test Message - " + i++);
                }
                break;
            default:
                System.out.println("Unknown mode selected");
        }

    }

    private static void singleAppMode() {
        SingleApplicationPlatform platform = new SingleApplicationPlatform("Test SingleApplicationPlatform");

        Player initiator = new Player("initiator").setStopCondition(new StopCondition(10));
        Player receiver = new Player("receiver");

        platform.registerPlayer(initiator);
        platform.registerPlayer(receiver);

        int i = 1;
        while (platform.isRunning()) {
            initiator.sendMessage(receiver.getUsername(), "Test Message - " + i++);
        }
    }
}
