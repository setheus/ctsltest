package umut.sahin.util;

import umut.sahin.player.RemotePlayer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.SocketAddress;

/**
 * Util class for serialization/deserialization and property extraction of io classes
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class IOUtils {
    public static byte[] serializeObject(Object obj) throws Exception {
        ByteArrayOutputStream bos = null;
        ObjectOutput out = null;
        try {
            bos = new ByteArrayOutputStream();
            out = new ObjectOutputStream(bos);
            out.writeObject(obj);
            out.flush();
            return bos.toByteArray();
        } finally {
            if (out != null) {
                out.close();
            }
            if (bos != null) {
                bos.close();
            }
        }
    }

    public static <T> T deserializeObject(byte[] byteArr) throws Exception {
        ByteArrayInputStream bis = null;
        ObjectInput in = null;
        try {
            bis = new ByteArrayInputStream(byteArr);
            in = new ObjectInputStream(bis);
            Object o = in.readObject();
            return (T) o;
        } finally {
            if (in != null) {
                in.close();
            }
            if (bis != null) {
                bis.close();
            }
        }
    }

    public static String getRemoteAddress(RemotePlayer remotePlayer){
        SocketAddress remoteAddress;
        try {
            remoteAddress = remotePlayer.getChannel().getRemoteAddress();
        } catch (Exception e) {
            return "";
        }
        return remoteAddress.toString();
    }
}
