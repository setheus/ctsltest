package umut.sahin.logic;

import umut.sahin.model.ConfirmationMessage;

/**
 * Checks weather a condition to stop the messaging process is met
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class StopCondition {
    private int maxCount;

    public StopCondition(int maxCount) {
        this.maxCount = maxCount;
    }

    public boolean check(ConfirmationMessage confirmationMessage) {
        return confirmationMessage.getCounter() == maxCount;
    }
}
