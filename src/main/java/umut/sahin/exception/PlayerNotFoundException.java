package umut.sahin.exception;

/**
 * @author u.sahin
 * @since 12/06/2017
 */
public class PlayerNotFoundException extends RuntimeException {
    public PlayerNotFoundException(String msg) {
        super(msg);
    }
}
