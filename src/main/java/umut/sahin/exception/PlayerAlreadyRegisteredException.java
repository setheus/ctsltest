package umut.sahin.exception;

/**
 * @author u.sahin
 * @since 12/06/2017
 */
public class PlayerAlreadyRegisteredException extends RuntimeException {
    public PlayerAlreadyRegisteredException(String message) {
        super(message);
    }
}
