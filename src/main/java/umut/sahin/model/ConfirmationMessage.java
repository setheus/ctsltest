package umut.sahin.model;

/**
 * Extended message class for confirmation of messages with a counter field
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class ConfirmationMessage extends Message {
    private static final long serialVersionUID = -4609389268602444021L;
    private int counter;

    public int getCounter() {
        return counter;
    }

    public ConfirmationMessage setCounter(int counter) {
        this.counter = counter;
        return this;
    }

    public static ConfirmationMessage wrap(Message message) {
        ConfirmationMessage confirmationMessage = new ConfirmationMessage();
        confirmationMessage.setReceiverUsername(message.getReceiverUsername())
                           .setMessage(message.getMessage())
                           .setSenderUsername(message.getSenderUsername());
        return confirmationMessage;
    }

    public AbstractMessage unwrap(){
        return new Message().setReceiverUsername(getReceiverUsername())
                            .setMessage(getMessage())
                            .setSenderUsername(getSenderUsername());
    }
}
