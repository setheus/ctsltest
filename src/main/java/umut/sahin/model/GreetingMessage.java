package umut.sahin.model;

import org.apache.commons.lang3.Validate;

/**
 * Greeting message is used for client/server after connecting/accepting connections in order to let other side that the connection is acknowledged
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class GreetingMessage extends AbstractMessage {

    private static final long serialVersionUID = 2737947771224983508L;

    @Override
    public void validate() {
        Validate.notBlank(senderUsername, "senderUsername");
    }
}
