package umut.sahin.model;

import org.apache.commons.lang3.Validate;

/**
 * StopMessage finishes the process on platforms
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class StopMessage extends AbstractMessage {
    private static final long serialVersionUID = 6317360840345350698L;

    @Override
    public void validate() {
        Validate.notBlank(senderUsername, "senderUsername");
    }
}
