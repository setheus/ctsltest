package umut.sahin.model;

import java.io.Serializable;

/**
 * Base for messaging class
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public abstract class AbstractMessage implements Serializable{
    protected String senderUsername;

    public String getSenderUsername() {
        return senderUsername;
    }

    public AbstractMessage setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
        return this;
    }

    public abstract void validate();
}
