package umut.sahin.model;

import org.apache.commons.lang3.Validate;

/**
 * Basic messaging class which holds a message, sender and a receiver username
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class Message extends AbstractMessage {
    private static final long serialVersionUID = -7817759730451924226L;

    protected String receiverUsername;
    protected String message;

    public String getReceiverUsername() {
        return receiverUsername;
    }

    public Message setReceiverUsername(String receiverUsername) {
        this.receiverUsername = receiverUsername;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Message setMessage(String message) {
        this.message = message;
        return this;
    }

    public void validate() {
        Validate.notBlank(senderUsername, "senderUsername");
        Validate.notBlank(receiverUsername, "receiverUsername");
        Validate.notBlank(message, "message");
    }
}
