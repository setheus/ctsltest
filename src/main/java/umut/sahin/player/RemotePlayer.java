package umut.sahin.player;

import umut.sahin.model.ConfirmationMessage;
import umut.sahin.model.Message;

import java.nio.channels.SocketChannel;

/**
 * RemotePlayer is the remote (server/client) version for the Player class
 * Which holds socketchannel and extended logging
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class RemotePlayer extends Player {

    private SocketChannel channel;

    public RemotePlayer(String username) {
        super(username);
    }

    public SocketChannel getChannel() {
        return channel;
    }

    public RemotePlayer setChannel(SocketChannel channel) {
        this.channel = channel;
        return this;
    }

    @Override
    protected void logConfirmationRead(ConfirmationMessage message) {
        System.out.println("# Confirmation\n" + message.getMessage() + "|" + message.getCounter() + "\n");
    }

    @Override
    protected void logMessageRead(Message message) {
        System.out.println("# Read message\n" + message.getSenderUsername() + ": " + message.getMessage() + "\n");
    }
}
