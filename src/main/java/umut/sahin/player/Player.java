package umut.sahin.player;

import org.apache.commons.lang3.Validate;
import umut.sahin.logic.StopCondition;
import umut.sahin.model.ConfirmationMessage;
import umut.sahin.model.Message;
import umut.sahin.model.StopMessage;
import umut.sahin.platform.Platform;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Base player class
 * receives and sends messages
 *
 * @author u.sahin
 * @since 12/06/2017
 */
public class Player {
    private final String username;
    private final Map<String, AtomicInteger> messageCounter = new HashMap<>();
    private Platform platform;
    private StopCondition stopCondition;

    public Player(String username) {
        Validate.notBlank(username, "Username is blank");
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public Player setStopCondition(StopCondition stopCondition) {
        this.stopCondition = stopCondition;
        return this;
    }

    public void receiveMessage(Message message) {
        Validate.notNull(message);
        if (ConfirmationMessage.class.isInstance(message)) {
            handleConfirmationMessage(message);
        } else {
            handleMessage(message);
        }
    }

    public void sendMessage(String receiver, String msg) {
        Validate.notBlank(receiver, "Receiver username is blank");
        Validate.notBlank(msg, "Message is blank");
        Validate.notNull(platform, "Platform is null");
        Validate.notNull(stopCondition, "stop condition doesn't exists");
        Message message = (Message) new Message().setReceiverUsername(receiver)
                                                 .setMessage(msg)
                                                 .setSenderUsername(username);
        platform.deliverMessage(message);
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Player player = (Player) o;

        return username.equals(player.username);
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    private int getUserMessageCounter(String senderUsername) {
        if (!messageCounter.containsKey(senderUsername)) {
            messageCounter.put(senderUsername, new AtomicInteger(0));
        }
        return messageCounter.get(senderUsername).incrementAndGet();
    }

    private void handleMessage(Message message) {
        String senderUsername = message.getSenderUsername();
        int updatedCount = getUserMessageCounter(senderUsername);
        logMessageRead(message);
        ConfirmationMessage confMsg = ConfirmationMessage.wrap(message)
                                                         .setCounter(updatedCount);
        platform.deliverMessage(confMsg);
    }

    private void handleConfirmationMessage(Message message) {
        ConfirmationMessage msg = ConfirmationMessage.class.cast(message);
        logConfirmationRead(msg);
        if (stopCondition.check(msg)) {
            platform.deliverMessage(new StopMessage().setSenderUsername(username));
            platform.stop();
        }
    }

    protected void logConfirmationRead(ConfirmationMessage message) {
        System.out.println("# Confirmation\n" + message.getSenderUsername() + " <- " +
                           message.getReceiverUsername() + ": " + message.getMessage() + "|" + message.getCounter() + "\n");
    }

    protected void logMessageRead(Message message) {
        System.out.println("# Message\n" + message.getSenderUsername() +
                           " -> " + message.getReceiverUsername() + ": " + message.getMessage() + "\n");
    }
}
