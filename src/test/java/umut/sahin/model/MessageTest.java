package umut.sahin.model;

import org.junit.Test;

/**
 * @author u.sahin
 * @since 12/06/2017
 */
public class MessageTest {
    @Test
    public void should_validate_message() {
        AbstractMessage message = new Message().setReceiverUsername("receiver")
                                                       .setMessage("test")
                                                       .setSenderUsername("sender");
        message.validate();
    }

    @Test(expected = NullPointerException.class)
    public void should_fail_validating_message_with_empty_fields(){
        AbstractMessage message = new Message().setSenderUsername("asd");
        message.validate();
    }
}