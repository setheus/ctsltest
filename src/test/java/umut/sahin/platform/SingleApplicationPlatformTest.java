package umut.sahin.platform;

import org.junit.Assert;
import org.junit.Test;
import umut.sahin.exception.PlayerAlreadyRegisteredException;
import umut.sahin.exception.PlayerNotFoundException;
import umut.sahin.logic.StopCondition;
import umut.sahin.model.AbstractMessage;
import umut.sahin.model.Message;
import umut.sahin.player.Player;

/**
 * @author u.sahin
 * @since 12/06/2017
 */
public class SingleApplicationPlatformTest {

    private static final String PLATFORM_NAME = "Platform1";

    @Test
    public void should_create_new_platform() {
        SingleApplicationPlatform platform = new SingleApplicationPlatform(PLATFORM_NAME);
        Assert.assertNotNull(platform);
        Assert.assertEquals(PLATFORM_NAME, platform.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_creating_platform_without_name() {
        new SingleApplicationPlatform("");
    }

    @Test
    public void should_register_player_to_platform() {
        Player player = new Player("test");
        SingleApplicationPlatform platform = new SingleApplicationPlatform(PLATFORM_NAME);
        platform.registerPlayer(player);
    }

    @Test(expected = PlayerAlreadyRegisteredException.class)
    public void should_fail_to_register_player_to_platform_if_username_already_registered() {
        Player player = new Player("test");
        SingleApplicationPlatform platform = new SingleApplicationPlatform(PLATFORM_NAME);
        platform.registerPlayer(player);
        platform.registerPlayer(player);
    }

    @Test
    public void should_send_valid_message_to_user() {
        SingleApplicationPlatform platform = new SingleApplicationPlatform(PLATFORM_NAME);

        Player sender = new Player("sender");
        sender.setStopCondition(new StopCondition(2));
        Player receiver = new Player("receiver");

        platform.registerPlayer(sender);
        platform.registerPlayer(receiver);

        AbstractMessage message = new Message()
                .setReceiverUsername(receiver.getUsername())
                .setMessage("test")
                .setSenderUsername(sender.getUsername());

        platform.deliverMessage(message);
    }

    @Test(expected = NullPointerException.class)
    public void should_fail_sending_invalid_message_to_player() {
        SingleApplicationPlatform platform = new SingleApplicationPlatform(PLATFORM_NAME);

        Player sender = new Player("sender");
        Player receiver = new Player("receiver");

        platform.registerPlayer(sender);
        platform.registerPlayer(receiver);

        AbstractMessage message = new Message().setReceiverUsername(receiver.getUsername())
                                               .setSenderUsername(sender.getUsername());

        platform.deliverMessage(message);
    }

    @Test(expected = PlayerNotFoundException.class)
    public void should_fail_sending_valid_message_to_non_registered_player() {
        SingleApplicationPlatform platform = new SingleApplicationPlatform(PLATFORM_NAME);

        Player sender = new Player("sender");

        platform.registerPlayer(sender);

        AbstractMessage message = new Message()
                .setReceiverUsername("notExistingPlayer")
                .setMessage("test")
                .setSenderUsername(sender.getUsername());

        platform.deliverMessage(message);
    }
}