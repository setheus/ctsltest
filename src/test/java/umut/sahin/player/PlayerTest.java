package umut.sahin.player;

import org.junit.Assert;
import org.junit.Test;
import umut.sahin.logic.StopCondition;
import umut.sahin.model.Message;
import umut.sahin.platform.SingleApplicationPlatform;

/**
 * @author u.sahin
 * @since 12/06/2017
 */
public class PlayerTest {

    private static final String USER_NAME = "Username";

    @Test
    public void should_create_new_player() {
        Player player = new Player(USER_NAME);
        Assert.assertNotNull(player);
        Assert.assertEquals(USER_NAME, player.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_fail_creating_player_if_username_is_null() {
        new Player("");
    }

    @Test
    public void should_receive_message_and_response() {
        Player receiver = new Player(USER_NAME);
        Player sender = new Player("sender");
        sender.setStopCondition(new StopCondition(2));
        Message message = (Message) new Message().setReceiverUsername(USER_NAME)
                                                 .setMessage("test message")
                                                 .setSenderUsername("sender");

        SingleApplicationPlatform platform = new SingleApplicationPlatform("test");
        platform.registerPlayer(receiver);
        platform.registerPlayer(sender);

        receiver.receiveMessage(message);
    }
}