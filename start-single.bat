@ECHO OFF
ECHO ### Building application...
call mvn -q clean install -Dmaven.test.skip=true
ECHO ### Building build successful.
TIMEOUT 3
ECHO.
ECHO ### Starting application in single mode...
ECHO.
java -jar "target\ctsltest-1.0-SNAPSHOT.jar" SINGLE
