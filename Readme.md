**CTSL TEST**
-

Author: Umut Sahin (umutshin@gmail.com)

* Running Modes:
    * Single Mode
    * Distributed Mode

**1. Single Mode**

On this mode application creates a platform instance and two players with stop condition of message count 10.

**2. Distributed Mode**

On this mode application creates a platform depending on the console args.

_SERVER_

On this mode application just acts as a bridge between other client platforms.

_CLIENT-RECEIVER_

On this mode application just creates a client platform and connects to server and waits for a message

_CLIENT-SENDER_

On this mode application creates a client platform and connects to server. 
Then sends messages until stop condition is met.
When a stop condition is met a stop message is sent to server. This ends all running clients and server.

**3. Technology Stack**

Used JUnit 4 for testing and Apache Commons Lang for boiler plate validations and checks.
   
On distributed mode used Java NIO sockets

And some java 8 streams.

**Examples**

_**Build**_
This command cleans & builds this project and runs tests.

`ex: mvn clean install`

_**Run Single Mode**_

This command runs the application on single instance mode.

`ex: java -jar "target\ctsltest-1.0-SNAPSHOT.jar" SINGLE`

_**Run Distributed Mode**_

Run these commands with the same order on different terminals

`ex: java -jar "target\ctsltest-1.0-SNAPSHOT.jar" SERVER`

`ex: java -jar "target\ctsltest-1.0-SNAPSHOT.jar" CLIENT-RECEIVER`

`ex: java -jar "target\ctsltest-1.0-SNAPSHOT.jar" CLIENT-SENDER`
