@ECHO OFF
ECHO ### Building application...
call mvn -q clean install -Dmaven.test.skip=true
ECHO ### Building build successful.
TIMEOUT 3
ECHO.
ECHO ### Starting application in distributed mode...
ECHO.
START /B java -jar "target\ctsltest-1.0-SNAPSHOT.jar" SERVER > NUL 2>&1
TIMEOUT 3
START /B java -jar "target\ctsltest-1.0-SNAPSHOT.jar" CLIENT-RECEIVER > NUL 2>&1
TIMEOUT 3
START /B java -jar "target\ctsltest-1.0-SNAPSHOT.jar" CLIENT-SENDER
